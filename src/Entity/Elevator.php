<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:07
 */

namespace App\Entity;


class Elevator
{
    /**
     * @var integer
     */
    private $floor = 0;

    /**
     * @var integer
     */
    private $countFloors = 0;

    /**
     * @var PositionTime[]
     */
    private $positions = [];

    /**
     * @return int
     */
    public function getFloor(): int
    {
        return $this->floor;
    }

    /**
     * @param int $floor
     */
    public function setFloor(int $floor): void
    {
        $this->countFloors = $this->countFloors + abs($this->floor - $floor);
        $this->floor = $floor;
    }

    /**
     * @return int
     */
    public function getCountFloors(): int
    {
        return $this->countFloors;
    }

    /**
     * @param int $countFloors
     */
    public function setCountFloors(int $countFloors): void
    {
        $this->countFloors = $countFloors;
    }

    /**
     * @return PositionTime[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param PositionTime[] $positions
     */
    public function setPositions(array $positions): void
    {
        $this->positions = $positions;
    }

    /**
     * @param PositionTime $positionTime
     */
    public function addPositions($positionTime): void
    {
        $this->positions[] = $positionTime;
    }


    public function addFloor(): void
    {
        $this->countFloors++;
    }

}

