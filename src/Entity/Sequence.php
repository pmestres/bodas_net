<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:07
 */

namespace App\Entity;


class Sequence
{
    /**
     * @var integer
     */
    private $lapse;

    /** @var \DateTime */
    private $startAt;

    /** @var \DateTime */
    private $endAt;

    /** @var array */
    private $startfloors;

    /** @var array */
    private $endfloors;

    /** @var int */
    private $totalMinutes = 0;

    /**
     * Sequence constructor.
     * @param int $lapse
     * @param $startAt
     * @param $endAt
     * @param $startfloors
     * @param $endfloors
     */
    public function __construct(int $lapse, $startAt, $endAt, $startfloors, $endfloors)
    {
        $this->lapse = $lapse;
        $this->startAt = \DateTime::createFromFormat("H:i", $startAt);
        $this->endAt = \DateTime::createFromFormat("H:i", $endAt);
        $this->startfloors = $startfloors;
        $this->endfloors = $endfloors;
    }

    /**
     * @return int
     */
    public function getLapse(): int
    {
        return $this->lapse;
    }

    /**
     * @param int $lapse
     */
    public function setLapse(int $lapse): void
    {
        $this->lapse = $lapse;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param mixed $startAt
     */
    public function setStartAt($startAt): void
    {
        $this->startAt = \DateTime::createFromFormat("H:i", $startAt);
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param mixed $endAt
     */
    public function setEndAt($endAt): void
    {
        $this->endAt = \DateTime::createFromFormat("H:i", $endAt);
    }

    /**
     * @return mixed
     */
    public function getStartfloors()
    {
        return $this->startfloors;
    }

    /**
     * @param mixed $startfloors
     */
    public function setStartfloors($startfloors): void
    {
        $this->startfloors = $startfloors;
    }

    /**
     * @return mixed
     */
    public function getEndfloors()
    {
        return $this->endfloors;
    }

    /**
     * @param mixed $endfloors
     */
    public function setEndfloors($endfloors): void
    {
        $this->endfloors = $endfloors;
    }

    /**
     * @return mixed
     */
    public function getTotalMinutes()
    {
        return $this->totalMinutes;
    }

    /**
     * @param mixed $totalMinutes
     */
    public function setTotalMinutes($totalMinutes): void
    {
        $this->totalMinutes = $totalMinutes;
    }

    /**
     * @param mixed $totalMinutes
     */
    public function addMinute(): void
    {
        $this->totalMinutes++;
    }


}

