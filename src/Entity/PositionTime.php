<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 18:25
 */

namespace App\Entity;


class PositionTime
{
    /** @var integer */
    private $floor;

    /** @var \DateTime */
    private $time;

    /**
     * PositionTime constructor.
     * @param int $floor
     * @param \DateTime $time
     */
    public function __construct(int $floor, \DateTime $time)
    {
        $this->floor = $floor;
        $this->time = $time;
    }

    /**
     * @return int
     */
    public function getFloor(): int
    {
        return $this->floor;
    }

    /**
     * @param int $floor
     */
    public function setFloor(int $floor): void
    {
        $this->floor = $floor;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time): void
    {
        $this->time = $time;
    }

}