<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:07
 */

namespace App\Entity;


class Report
{
    /**
     * @var integer
     */
    private $elevatorIndex;

    /**
     * @var integer
     */
    private $countFloors;

    /**
     * @var PositionTime[]
     */
    private $positions = [];

    /**
     * @return int
     */
    public function getElevatorIndex(): int
    {
        return $this->elevatorIndex;
    }

    /**
     * @param int $elevatorIndex
     */
    public function setElevatorIndex(int $elevatorIndex): void
    {
        $this->elevatorIndex = $elevatorIndex;
    }

    /**
     * @return int
     */
    public function getCountFloors(): int
    {
        return $this->countFloors;
    }

    /**
     * @param int $countFloors
     */
    public function setCountFloors(int $countFloors): void
    {
        $this->countFloors = $countFloors;
    }

    /**
     * @return PositionTime[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param PositionTime[] $positions
     */
    public function setPositions(array $positions): void
    {
        $this->positions = $positions;
    }

    /**
     * @param PositionTime $positionTime
     */
    public function addPositions($positionTime): void
    {
        $this->positions[] = $positionTime;
    }

}

