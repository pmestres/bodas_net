<?php

namespace App\Controller;


use App\Entity\Elevator;
use App\Entity\Sequence;
use App\Service\ReportService;
use App\Service\SimulatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/simulator/run", name="app_simulator_run")
     * @param SimulatorService $simulatorService
     * @param ReportService $reportService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function run(SimulatorService $simulatorService)
    {
        $startAt = "09:00";
        $endAt = "20:00";

        $sequences[] = new Sequence(5,"09:00", "11:00",[0],[2]);
        $sequences[] = new Sequence(10,"09:00", "10:00",[0],[1]);
        $sequences[] = new Sequence(20,"11:00", "18:20",[0],[1,2,3]);
        $sequences[] = new Sequence(4,"14:00", "15:00",[1,2],[3]);

        $elevators[] = new Elevator();
        $elevators[] = new Elevator();
        $elevators[] = new Elevator();
        $elevators[] = new Elevator();

        $elevators = $simulatorService->execute($startAt, $endAt, $sequences, $elevators);

        return $this->render('simulator/report.html.twig', [
            'elevators' => $elevators,
        ]);
    }

}