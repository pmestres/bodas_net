<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:09
 */

namespace App\Service;


class FakeElevatorService
{
    /**
     * @param $from
     * @param $to
     * @param $numElevators
     * @return int
     * @throws \Exception
     */
    public function call($from, $to, $numElevators){
        return random_int(0, $numElevators-1);
    }
}