<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:00
 */

namespace App\Service;


use App\Entity\Elevator;
use App\Entity\PositionTime;
use App\Entity\Report;
use App\Entity\Sequence;

class ReportService
{

    public function make($startAt, $endAt, $elevators){

        $indexDatetime = \DateTime::createFromFormat("H:i", $startAt);
        $endDatetime = \DateTime::createFromFormat("H:i", $endAt);
        $report = [];
        $time = [];
        $index = 1;
        /** @var Elevator $elevator */
        foreach ($elevators as $elevator){
            $report = new Report();
            $report->setElevatorIndex($index);
            $report->setCountFloors($elevator->getCountFloors());

            while ($indexDatetime < $endDatetime) {
                $report->addPositions(new PositionTime(0, new \DateInterval('PT' . self::TIME_TRAVEL . 'M')));
                $indexDatetime->add(new \DateInterval('PT' . 1 . 'M'));
            }

            $report[] = new $report;

        }

        return $elevators;
    }
}