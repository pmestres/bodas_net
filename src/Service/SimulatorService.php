<?php
/**
 * Created by PhpStorm.
 * User: peremestresdomenech
 * Date: 20/03/2020
 * Time: 17:00
 */

namespace App\Service;


use App\Entity\Elevator;
use App\Entity\PositionTime;
use App\Entity\Sequence;

class SimulatorService
{

    /** @var FakeElevatorService $fakeElevatorService */
    private $fakeElevatorService;

    private const TIME_TRAVEL = 1;

    /**
     * SimulatorService constructor.
     * @param FakeElevatorService $fakeElevatorService
     */
    public function __construct(FakeElevatorService $fakeElevatorService)
    {
        $this->fakeElevatorService = $fakeElevatorService;
    }

    /**
     * @param $startAt
     * @param $endAt
     * @param $sequenceArray
     * @param $elevators
     * @return mixed
     * @throws \Exception
     */
    public function execute($startAt, $endAt, $sequenceArray, $elevators){

        $startDatetime = \DateTime::createFromFormat("H:i", $startAt);
        $endDatetime = \DateTime::createFromFormat("H:i", $endAt);
        $indexDatetime = \DateTime::createFromFormat("H:i", $startAt);

        while ($indexDatetime < $endDatetime) {

            /** @var Sequence $sequence */
            foreach ($sequenceArray as $sequence) {

                if ($sequence->getStartAt() <= $indexDatetime && $sequence->getEndAt() >= $indexDatetime) {

                    if ($sequence->getTotalMinutes() % $sequence->getLapse() == 0){

                        foreach ($sequence->getStartfloors() as $startFloor) {

                            foreach ($sequence->getEndfloors() as $endFloor) {
                                $elevatorIndex = $this->fakeElevatorService->call($startFloor, $endFloor, count($elevators));

                                /** @var Elevator $elevator */
                                $elevator = $elevators[$elevatorIndex];

                                $timeTravel = clone $indexDatetime;

                                if (count($elevator->getPositions()) > 0) {
                                    $lastPosition = $elevator->getPositions()[count($elevator->getPositions())-1];

                                    if ($indexDatetime > $lastPosition->getTime()) {
                                        $timeTravel = clone $lastPosition->getTime();

                                    }
                                }

                                if ($startFloor != $elevator->getFloor())
                                    $timeTravel = $timeTravel->add(new \DateInterval('PT' . self::TIME_TRAVEL . 'M'));

                                $elevator->addPositions(new PositionTime($startFloor, $timeTravel));

                                $elevator->setFloor($startFloor);
                                $elevator->setFloor($endFloor);
                                $elevator->addPositions(new PositionTime($endFloor, (clone $timeTravel)->add(new \DateInterval('PT' . self::TIME_TRAVEL . 'M'))));
                            }

                        }

                    }

                    $sequence->addMinute();
                }

            }

            $indexDatetime->add(new \DateInterval('PT' . 1 . 'M'));
        }

        return $elevators;
    }
}